import datetime
import locale

locale.setlocale(locale.LC_TIME, 'ru_RU')
date_str = input('Введите дату в фломате ГГГГ-ММ-ДД (2000-10-06): ')

try:
    date_object = datetime.datetime.strptime(date_str, "%Y-%m-%d")
    formatted_date = date_object.strftime("%d %B %Y")
    print('Верный формат даты: ', formatted_date)

except ValueError:
    print('Некорректный формат даты. Пожалуйста, введите дату снова в формате ГГГГ-ММ-ДД')


    # fghjkl